#!/bin/sh

set -e

current_dir=`pwd`
keyfile="./${HOST}.key"

if [ -z "${HOST}" ]; then
  echo "HOST env var needs to be set"
  exit 1
fi

if [ ! -f "${keyfile}" ]; then
  echo "Key file ${keyfile} does not exist"
  exit 1
fi

echo "Expiring backups for: ${HOST}"
cache_dir="${current_dir}/cache/${HOST}"
mkdir -p cache_dir
mkdir ${HOST}

echo "Copying tarsnap config …"
scp \
  -rq ${HOST}:/usr/local/etc/tarsnapp\* \
  ${HOST}/

echo "Updating tarsnap config …"
sed -i.save \
  "s|/usr/local/etc|${current_dir}/${HOST}|g" \
  ./${HOST}/tarsnapper.yml

echo "Running fsck …"
tarsnap \
  --fsck \
  --keyfile ${keyfile} \
  --cachedir ${cache_dir}

echo "Expiring backups …"
tarsnapper \
  -o keyfile=${keyfile} \
  -o cachedir=${cache_dir} \
  --config ./${HOST}/tarsnapper.yml \
  expire

rm -rf ${HOST}* cache
